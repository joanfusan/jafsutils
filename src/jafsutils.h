/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_JAFSUTILS_H_
#define SRC_JAFSUTILS_H_

//!
//! @file main.cpp
//!
//! Util library
//! @author  Jose Antonio Fuentes Santiago
//! @version 1.0
//!
//! @mainpage Util library
//!
//! <h2>General Information</h2>
//! <b>Author</b>: Jose Antonio Fuentes Santiago<br />
//! <b>Version</b>: 1.0<br />
//! <b>Year</b>: 2013
//!
//! <h2>Description</h2>
//! <p>This library provides a variety of utilities to all projects that linked
//! it.</p>
//!
//! <h2>Changelog</h2>
//! <h3>1.0 - 26/11/2013</h3>
//! <ul><li>Initial version.</li></ul>
//!
#include <string>

using std::string;

#if __WORDSIZE == 64 || defined (_Win64)
#define JAFS_64
#else
#define JAFS_32
#endif

#ifdef __linux__
#define JAFS_LINUX
#else
#define JAFS_WIN
#endif


//!
//! @brief  Returns a string with current library version.
//! @return String with current library version.
//!
string utils_library_version();

#endif  // SRC_JAFSUTILS_H_
