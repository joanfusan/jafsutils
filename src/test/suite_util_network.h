/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_TEST_SUITE_UTIL_NETWORK_H_
#define SRC_TEST_SUITE_UTIL_NETWORK_H_

//!
//! Initializes the util network suite.
//! @return 0 on success, non-zero otherwise.
//!
int initUtilNetwork();


//!
//! Tests for arrayToHex function.
//!
void tun_arrayToHex();

//!
//! Tests for getIp function.
//!
void tun_getIp();

//!
//! Tests for isValidIp function.
//!
void tun_isValidIp();

//!
//! Tests for getInterfaces function.
//!
void tun_getInterfaces();

//!
//! Tests for getMac function.
//!
void tun_getMac();

#endif  // SRC_TEST_SUITE_UTIL_NETWORK_H_
