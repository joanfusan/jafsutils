/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_TEST_SUITE_UTIL_SYSTEM_H_
#define SRC_TEST_SUITE_UTIL_SYSTEM_H_

//!
//! Initializes the util system suite.
//! @return 0 on success, non-zero otherwise.
//!
int initUtilSystem();

//!
//! Tests for sspleep function.
//!
void tusy_ssleep();

//!
//! Tests for savePid function.
//!
void tusy_savePid();

//!
//! Tests for executeCommand function.
//!
void tusy_executeCommand();

#endif  // SRC_TEST_SUITE_UTIL_SYSTEM_H_
