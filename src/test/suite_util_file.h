/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_TEST_SUITE_UTIL_FILE_H_
#define SRC_TEST_SUITE_UTIL_FILE_H_

//!
//! Initializes the util file suite.
//! @return 0 on success, non-zero otherwise.
//!
int initUtilFile();

//!
//! Closes the temporary file used by the tests.
//! @return 0 on success, non-zero otherwise.
//!
int cleanUtilFile();

//!
//! Tests for fsize function.
//!
void tuf_fsize();

//!
//! Tests for fexists function.
//!
void tuf_fexists();

#endif  // SRC_TEST_SUITE_UTIL_FILE_H_
