/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./suite_util_system.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <CUnit/CUnit.h>

#include "../objects/date.h"
#include "../utils/util_system.h"


// ************************************************************************** //
// INIT_UTIL_SYSTEM                                                           //
// ************************************************************************** //
int initUtilSystem() {
  return 0;
}


// ************************************************************************** //
// TUSY_SSLEEP                                                                //
// ************************************************************************** //
void tusy_ssleep() {
  Date start;
  util::ssleep(100);
  Date end;

  // Always lost some precission.                                             //
  double time = end - start;
  CU_ASSERT_TRUE(time > 0.099);

  start.update();
  util::ssleep(50);
  end.update();

  // Always lost some precission.                                             //
  time = end - start;
  CU_ASSERT_TRUE(time > 0.049);
}


// ************************************************************************** //
// TUSY_SAVE_PID                                                              //
// ************************************************************************** //
void tusy_savePid() {
  char current[1024];
  if (getcwd(current, sizeof(current))) {
    string currentDir = current;
    currentDir += "/test.pid";

    FILE * file = fopen(currentDir.c_str(), "w");
    if (file) {
      fclose(file);

      util::savePid(currentDir);
      file = fopen(currentDir.c_str(), "r");
      if (file) {
        char content[32];
        if (fgets(content, sizeof(content), file)) {
          CU_ASSERT_EQUAL(getpid(), atoi(content));
        } else {
          CU_ASSERT_TRUE(false);
        }
        fclose(file);
      }
    }
  }
}


// ************************************************************************** //
// TUSY_EXECUTE_COMMAND                                                       //
// ************************************************************************** //
void tusy_executeCommand() {

}
