/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./suite_util_file.h"

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <CUnit/CUnit.h>

#include "../utils/util_file.h"


// Constants.                                                                 //
//! Name of result file.                                                      //
static const char FILE_NAME[] = "test.txt";
//! Name of result directory.                                                 //
static const char DIR_NAME[] = "test";
//! Current directory identifier.                                             //
static const char CURRENT_DIR[] = ".";
//! Name of result file that not exists.                                      //
static const char FILE_NAME_ERR[] = "not_exists.txt";
//! Content of the file.                                                      //
static const char FILE_CONTENT[] = "abcdefghijklmnopqrstuvwxyz";
//! Minimun directory size.                                                   //
static const size_t MIN_DIR_SIZE = 4096;


// ************************************************************************** //
// INIT_UTIL_FILE                                                             //
// ************************************************************************** //
int initUtilFile() {
  int result = 0;

  // Writes the file content and makes the directory.                         //
  FILE * file = fopen(FILE_NAME, "w");
  if (file) {
    fprintf(file, "%s", FILE_CONTENT);
    fclose(file);
  } else {
    result = 1;
  }

#ifdef JAFS_LINUX
  mkdir(DIR_NAME, 0777);
#else
  mkdir(DIR_NAME);
#endif

  // Ensures that not existing file is deleted.                               //
  remove(FILE_NAME_ERR);

  return result;
}


// ************************************************************************** //
// CLEAN_UTIL_FILE                                                            //
// ************************************************************************** //
int cleanUtilFile() {
  remove(FILE_NAME);
  remove(FILE_NAME_ERR);
  rmdir(DIR_NAME);

  return 0;
}

// ************************************************************************** //
// TUF_FSIZE                                                                  //
// ************************************************************************** //
void tuf_fsize() {
  size_t size;

  // Empty file name.                                                         //
  CU_ASSERT_FALSE(util::fsize("", size));

  // Not existing file.                                                       //
  CU_ASSERT_FALSE(util::fsize(FILE_NAME_ERR, size));

  // Existing file.                                                           //
  CU_ASSERT_FATAL(util::fsize(FILE_NAME, size));
  CU_ASSERT_EQUAL(size, strlen(FILE_CONTENT));

#ifdef JAFS_LINUX
  // Existing directory.                                                      //
  CU_ASSERT_FATAL(util::fsize(DIR_NAME, size));
  CU_ASSERT(size >= MIN_DIR_SIZE);

  CU_ASSERT_FATAL(util::fsize(CURRENT_DIR, size));
  CU_ASSERT(size >= MIN_DIR_SIZE);
#endif
}


// ************************************************************************** //
// TUS_FEXISTS                                                                //
// ************************************************************************** //
void tuf_fexists() {
  // Empty file name.                                                         //
  CU_ASSERT_FALSE(util::fexists(""));

  // Not existing file.                                                       //
  CU_ASSERT_FALSE(util::fexists(FILE_NAME_ERR));

  // Existing file and directory.                                             //
  CU_ASSERT_TRUE(util::fexists(FILE_NAME));
  CU_ASSERT_TRUE(util::fexists(DIR_NAME));
  CU_ASSERT_TRUE(util::fexists(CURRENT_DIR));
}
