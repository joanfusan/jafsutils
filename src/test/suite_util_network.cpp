/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./suite_util_file.h"

#include <CUnit/CUnit.h>
#include <sys/stat.h>
#include <string.h>

#include "../utils/util_network.h"


// Constants.                                                                 //
//! Example string with text.                                                 //
static const char TEXT1[] = "0123";
//! Hexadecimal result of text 1.                                             //
static const char TEXT1_HEX[] = "30 31 32 33";
//! Size of text 1.                                                           //
static const size_t TEXT1_SIZE = 4;
//! Example string with text 2.                                               //
static const char TEXT2[] = {char(0x1A), char(0x2B), char(0x04), char(0x15),
                             char(0xCD)};
//! Hexadecimal result of text 2.                                             //
static const char TEXT2_HEX[] = "1A 2B 04 15 CD";
//! End position of text 2.                                                   //
static const size_t TEXT2_END = 5;
//! Hexadecimal result of text 2, variant a.                                  //
static const char TEXT2a_HEX[] = "1A 2B";
//! End position of text 2, variant a.                                        //
static const size_t TEXT2a_END = 2;
//! Hexadecimal result of text 2, variant b.                                  //
static const char TEXT2b_HEX[] = "2B 04 15";
//! End position of text 2, variant b.                                        //
static const size_t TEXT2b_END = 4;


// ************************************************************************** //
// INIT_UTIL_NETWORK                                                          //
// ************************************************************************** //
int initUtilNetwork() {
  return 0;
}


// ************************************************************************** //
// TUN_ARRAY_TO_HEX                                                           //
// ************************************************************************** //
void tun_arrayToHex() {
  char * data = NULL;

  // Empty data and wrong index.                                              //
  CU_ASSERT(util::arrayToHex(data, 0, 0).empty());
  CU_ASSERT(util::arrayToHex(data, 0, 10).empty());
  CU_ASSERT(util::arrayToHex(data, 10, 0).empty());
  data = const_cast<char *> (TEXT1);
  CU_ASSERT(util::arrayToHex(data, 10, 0).empty());

  CU_ASSERT(util::arrayToHex(data, 0, TEXT1_SIZE) == TEXT1_HEX);

  data = const_cast<char *> (TEXT2);
  CU_ASSERT(util::arrayToHex(data, 0, TEXT2_END) == TEXT2_HEX);
  CU_ASSERT(util::arrayToHex(data, 0, TEXT2a_END) == TEXT2a_HEX);
  CU_ASSERT(util::arrayToHex(data, 1, TEXT2b_END) == TEXT2b_HEX);
}
