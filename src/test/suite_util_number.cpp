/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./suite_util_number.h"

#include <CUnit/CUnit.h>
#include <string>

#include "../utils/util_number.h"

using std::string;

// Constants.                                                                 //
//! Array that composed a signed short value.                                 //
char AR_SHORT[] = {0x34, 0x12};
//! Value of signed short value.                                              //
const int16_t V_SHORT = 0x1234;
//! Value of signed short value when gets the reverse.                        //
const int16_t V_SHORT_R = 0x3412;
//! Array that composed a unsigned short value.                               //
char AR_SHORT_U[] = {0x34, 0x12};
//! Value of unsigned short value.                                            //
const uint16_t V_SHORT_U = 0x1234;
//! Array that composed a signed integer value.                               //
char AR_INTEGER[] = {0x78, 0x56, 0x34, 0x12};
//! Value of signed integer value.                                            //
const int32_t V_INTEGER = 0x12345678;
//! Value of signed integer value when gets the reverse.                      //
const int32_t V_INTEGER_R = 0x78563412;
//! Array that composed a unsigned integer value.                             //
char AR_INTEGER_U[] = {char(0x21), char(0x43), char(0x65), char(0x87)};
//! Value of unsigned integer value.                                          //
const uint32_t V_INTEGER_U = 0x87654321;
//! Array that composed a signed long value.                                  //
char AR_LONG[] = {char(0xF0), char(0xDE), char(0xBC), char(0x9A), char(0x78),
                  char(0x56), char(0x34), char(0x12)};
//! Value of signed long value.                                               //
const int64_t V_LONG = 0x123456789ABCDEF0LL;
//! Array that uncomposed a signed long value.                                //
char AR_LONG_U[] = {char(0x10), char(0x32), char(0x54), char(0x76), char(0x98),
                    char(0xBA), char(0xDC), char(0xFE)};
//! Value of unsigned long value.                                             //
const uint64_t V_LONG_U = 0xFEDCBA9876543210ULL;


// ************************************************************************** //
// INIT_UTIL_NUMBER                                                           //
// ************************************************************************** //
int initUtilNumber() {
  return 0;
}


// ************************************************************************** //
// TUN_ARRAY_TO_INTEGER                                                       //
// ************************************************************************** //
void tun_arrayToInteger() {
  int16_t svalue = 0;
  CU_ASSERT_FALSE(util::arrayToInteger(NULL, 0, svalue));
  CU_ASSERT_FALSE(util::arrayToInteger(NULL, 10, svalue));
  CU_ASSERT_FALSE(util::arrayToInteger(AR_SHORT, 0, svalue));
  CU_ASSERT_FALSE(util::arrayToInteger(AR_SHORT, 1, svalue));

  // Short tests                                                              //
  CU_ASSERT_TRUE(util::arrayToInteger(AR_SHORT, sizeof(AR_SHORT), svalue));
  CU_ASSERT_EQUAL(svalue, V_SHORT);

  CU_ASSERT_TRUE(util::arrayToInteger(AR_SHORT, sizeof(AR_SHORT), svalue,
                                      true));
  CU_ASSERT_EQUAL(svalue, V_SHORT_R);

  // Unsigned short tests                                                     //
  uint16_t usvalue = 0;
  CU_ASSERT_TRUE(util::arrayToInteger(AR_SHORT_U, sizeof(AR_SHORT_U), usvalue));
  CU_ASSERT_EQUAL(usvalue, V_SHORT_U);

  // Integer tests                                                            //
  int32_t ivalue = 0;
  CU_ASSERT_TRUE(util::arrayToInteger(AR_INTEGER, sizeof(AR_INTEGER), ivalue));
  CU_ASSERT_EQUAL(ivalue, V_INTEGER);

  CU_ASSERT_TRUE(util::arrayToInteger(AR_INTEGER, sizeof(AR_INTEGER), ivalue,
                                      true));
  CU_ASSERT_EQUAL(ivalue, V_INTEGER_R);

  // Unsigned integer tests                                                   //
  uint32_t uivalue = 0;
  CU_ASSERT_TRUE(util::arrayToInteger(AR_INTEGER_U, sizeof(AR_INTEGER_U),
                 uivalue));
  CU_ASSERT_EQUAL(uivalue, V_INTEGER_U);

  // Long tests                                                               //
  int64_t lvalue = 0;
  CU_ASSERT_TRUE(util::arrayToInteger(AR_LONG, sizeof(AR_LONG), lvalue));
  CU_ASSERT_TRUE(lvalue == V_LONG);

  // Unsigned long tests                                                      //
  uint64_t ulvalue = 0;
  CU_ASSERT_TRUE(util::arrayToInteger(AR_LONG_U, sizeof(AR_LONG_U), ulvalue));
  CU_ASSERT_TRUE(ulvalue == V_LONG_U);


  // Not integer tests                                                        //
  char array[16];
  memset(array, 0, sizeof(array));

  string stringValue;
  CU_ASSERT_FALSE(util::arrayToInteger(array, sizeof(array), stringValue));

  double dvalue;
  CU_ASSERT_FALSE(util::arrayToInteger(array, sizeof(array), dvalue));

  float fvalue;
  CU_ASSERT_FALSE(util::arrayToInteger(array, sizeof(array), fvalue));
}


// ************************************************************************** //
// TUN_IS_INTEGER                                                             //
// ************************************************************************** //
void tun_isInteger() {
  char array[8];
  CU_ASSERT_FALSE(util::isInteger(array));
  char character;
  CU_ASSERT_FALSE(util::isInteger(character));
  string stringValue;
  CU_ASSERT_FALSE(util::isInteger(stringValue));
  double doubleValue;
  CU_ASSERT_FALSE(util::isInteger(doubleValue));
  float floatValue;
  CU_ASSERT_FALSE(util::isInteger(floatValue));

  int16_t shortValue;
  CU_ASSERT_TRUE(util::isInteger(shortValue));
  uint16_t usValue;
  CU_ASSERT_TRUE(util::isInteger(usValue));
  int32_t intValue;
  CU_ASSERT_TRUE(util::isInteger(intValue));
  uint32_t uiValue;
  CU_ASSERT_TRUE(util::isInteger(uiValue));
  int64_t longValue;
  CU_ASSERT_TRUE(util::isInteger(longValue));
  uint64_t ulValue;
  CU_ASSERT_TRUE(util::isInteger(ulValue));
}


// ************************************************************************** //
// TUN_BTOS                                                                   //
// ************************************************************************** //
void tun_btos() {
  CU_ASSERT_STRING_EQUAL(util::btos(true).c_str(), "true");
  CU_ASSERT_STRING_EQUAL(util::btos(2).c_str(), "true");
  CU_ASSERT_STRING_EQUAL(util::btos(false).c_str(), "false");
  CU_ASSERT_STRING_EQUAL(util::btos(0).c_str(), "false");
}


// ************************************************************************** //
// TUN_ITOS                                                                   //
// ************************************************************************** //
void tun_itos() {
  CU_ASSERT_STRING_EQUAL(util::itos(-1).c_str(), "-1");
  CU_ASSERT_STRING_EQUAL(util::itos(0).c_str(), "0");
  CU_ASSERT_STRING_EQUAL(util::itos(1).c_str(), "1");
  CU_ASSERT_STRING_EQUAL(util::itos(2147483646).c_str(), "2147483646");
  CU_ASSERT_STRING_EQUAL(util::itos(-2147483646).c_str(), "-2147483646");
}


// ************************************************************************** //
// TUN_LTOS                                                                   //
// ************************************************************************** //
void tun_ltos() {
  CU_ASSERT_STRING_EQUAL(util::ltos(-1).c_str(), "-1");
  CU_ASSERT_STRING_EQUAL(util::ltos(0).c_str(), "0");
  CU_ASSERT_STRING_EQUAL(util::ltos(1).c_str(), "1");
  CU_ASSERT_STRING_EQUAL(util::ltos(3147483646LL).c_str(), "3147483646");
  CU_ASSERT_STRING_EQUAL(util::ltos(-3147483646LL).c_str(), "-3147483646");
}


// ************************************************************************** //
// TUN_UTOS                                                                   //
// ************************************************************************** //
void tun_utos() {
  // -1 is 0xFFFFFFFF, and we have an unsigned integer.                       //
  CU_ASSERT_STRING_EQUAL(util::utos(-1).c_str(), "4294967295");
  CU_ASSERT_STRING_EQUAL(util::utos(0).c_str(), "0");
  CU_ASSERT_STRING_EQUAL(util::utos(1).c_str(), "1");
  CU_ASSERT_STRING_EQUAL(util::utos(2147483646).c_str(), "2147483646");
  CU_ASSERT_STRING_EQUAL(util::utos(4294967295U).c_str(), "4294967295");
}


// ************************************************************************** //
// TUN_DTOS                                                                   //
// ************************************************************************** //
void tun_dtos() {
  // Only show decimal part when result is not integer.                       //
  CU_ASSERT_STRING_EQUAL(util::dtos(-1).c_str(), "-1");
  CU_ASSERT_STRING_EQUAL(util::dtos(0).c_str(), "0");
  CU_ASSERT_STRING_EQUAL(util::dtos(1).c_str(), "1");
  CU_ASSERT_STRING_EQUAL(util::dtos(2147483646.345).c_str(),
                         "2147483646.345000");
  CU_ASSERT_STRING_EQUAL(util::dtos(4294967295.456).c_str(),
                         "4294967295.456000");

  // 6 decimal numbers.                                                       //
  CU_ASSERT_STRING_EQUAL(util::dtos(-1, 6).c_str(), "-1.000000");
  CU_ASSERT_STRING_EQUAL(util::dtos(0, 6).c_str(), "0.000000");
  CU_ASSERT_STRING_EQUAL(util::dtos(1, 6).c_str(), "1.000000");
  CU_ASSERT_STRING_EQUAL(util::dtos(2147483646.345, 6).c_str(),
                         "2147483646.345000");
  CU_ASSERT_STRING_EQUAL(util::dtos(4294967295.456, 6).c_str(),
                         "4294967295.456000");

  // 5 decimal numbers.                                                       //
  CU_ASSERT_STRING_EQUAL(util::dtos(-1, 5).c_str(), "-1.00000");
  CU_ASSERT_STRING_EQUAL(util::dtos(0, 5).c_str(), "0.00000");
  CU_ASSERT_STRING_EQUAL(util::dtos(1, 5).c_str(), "1.00000");
  CU_ASSERT_STRING_EQUAL(util::dtos(2147483646.345, 5).c_str(),
                         "2147483646.34500");
  CU_ASSERT_STRING_EQUAL(util::dtos(4294967295.456, 5).c_str(),
                         "4294967295.45600");

  // 4 decimal numbers.                                                       //
  CU_ASSERT_STRING_EQUAL(util::dtos(-1, 4).c_str(), "-1.0000");
  CU_ASSERT_STRING_EQUAL(util::dtos(0, 4).c_str(), "0.0000");
  CU_ASSERT_STRING_EQUAL(util::dtos(1, 4).c_str(), "1.0000");
  CU_ASSERT_STRING_EQUAL(util::dtos(2147483646.345, 4).c_str(),
                         "2147483646.3450");
  CU_ASSERT_STRING_EQUAL(util::dtos(4294967295.456, 4).c_str(),
                         "4294967295.4560");

  // 3 decimal numbers.                                                       //
  CU_ASSERT_STRING_EQUAL(util::dtos(-1, 3).c_str(), "-1.000");
  CU_ASSERT_STRING_EQUAL(util::dtos(0, 3).c_str(), "0.000");
  CU_ASSERT_STRING_EQUAL(util::dtos(1, 3).c_str(), "1.000");
  CU_ASSERT_STRING_EQUAL(util::dtos(2147483646.345, 3).c_str(),
                         "2147483646.345");
  CU_ASSERT_STRING_EQUAL(util::dtos(4294967295.456, 3).c_str(),
                         "4294967295.456");

  // 2 decimal numbers.                                                       //
  CU_ASSERT_STRING_EQUAL(util::dtos(-1, 2).c_str(), "-1.00");
  CU_ASSERT_STRING_EQUAL(util::dtos(0, 2).c_str(), "0.00");
  CU_ASSERT_STRING_EQUAL(util::dtos(1, 2).c_str(), "1.00");
  CU_ASSERT_STRING_EQUAL(util::dtos(2147483646.345, 2).c_str(),
                         "2147483646.34");
  CU_ASSERT_STRING_EQUAL(util::dtos(4294967295.456, 2).c_str(),
                         "4294967295.45");

  // 1 decimal numbers.                                                       //
  CU_ASSERT_STRING_EQUAL(util::dtos(-1, 1).c_str(), "-1.0");
  CU_ASSERT_STRING_EQUAL(util::dtos(0, 1).c_str(), "0.0");
  CU_ASSERT_STRING_EQUAL(util::dtos(1, 1).c_str(), "1.0");
  CU_ASSERT_STRING_EQUAL(util::dtos(2147483646.345, 1).c_str(),
                         "2147483646.3");
  CU_ASSERT_STRING_EQUAL(util::dtos(4294967295.456, 1).c_str(),
                         "4294967295.4");

  // No decimal numbers.                                                      //
  CU_ASSERT_STRING_EQUAL(util::dtos(-1, 0).c_str(), "-1");
  CU_ASSERT_STRING_EQUAL(util::dtos(0, 0).c_str(), "0");
  CU_ASSERT_STRING_EQUAL(util::dtos(1, 0).c_str(), "1");
  CU_ASSERT_STRING_EQUAL(util::dtos(2147483646.345, 0).c_str(),
                         "2147483646");
  CU_ASSERT_STRING_EQUAL(util::dtos(4294967295.456, 0).c_str(),
                         "4294967295");
}


// ************************************************************************** //
// TUN_OCTAL                                                                  //
// ************************************************************************** //
void tun_octal() {
  CU_ASSERT_STRING_EQUAL("0", util::octal(0).c_str());
  CU_ASSERT_STRING_EQUAL("0123", util::octal(0123).c_str());
  CU_ASSERT_STRING_EQUAL("07777", util::octal(07777).c_str());
  CU_ASSERT_STRING_EQUAL("01", util::octal(1).c_str());
}
