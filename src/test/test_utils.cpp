/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include <stdio.h>
#include <CUnit/Automated.h>
#include <CUnit/Basic.h>

#include "./suite_util_file.h"
#include "./suite_util_string.h"
#include "./suite_util_number.h"
#include "./suite_util_system.h"
#include "./suite_util_network.h"


//!
//! Makes the test suite for strings.
//!
void testUtilString();

//!
//! Makes the test suite for numbers.
//!
void testUtilNumber();

//!
//! Makes the test suite for file.
//!
void testUtilFile();

//!
//! Makes the test suite for network.
//!
void testUtilNetwork();

//!
//! Makes the test suite for system.
//!
void testUtilSystem();


//!
//! @brief  Startup function for launch the tests.
//! @param  argc  Arguments number.
//! @param  argv  Application arguments.
//! @return Execution result.
//!
int main(int argc, char * argv[]) {
  int result = CUE_SUCCESS;

  // Initializes the CUnit tests registry.                                    //
  if (CUE_SUCCESS == CU_initialize_registry()) {
    testUtilString();
    testUtilNumber();
    testUtilFile();
    testUtilNetwork();
    testUtilSystem();

    // Executes all the tests suite created.                                  //
    CU_automated_run_tests();
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();

    CU_cleanup_registry();
    result = CU_get_error();
  } else {
    result = CU_get_error();
  }

  return result;
}


// ************************************************************************** //
// TEST_UTIL_STRING                                                           //
// ************************************************************************** //
void testUtilString() {
  // Adds the suite to general tests suite.                                   //
  CU_pSuite suite = CU_add_suite("String Utils Suite", initUtilString, NULL);
  if (!suite ||
      !CU_add_test(suite, "Trim strings", tus_trim) ||
      !CU_add_test(suite, "Split strings", tus_split) ||
      !CU_add_test(suite, "Characters replace", tus_replace) ||
      !CU_add_test(suite, "Index of substring", tus_indexOf) ||
      !CU_add_test(suite, "String to map, and map to string", tus_stm) ||
      !CU_add_test(suite, "Lowercase", tus_lowercase) ||
      !CU_add_test(suite, "Uppercase", tus_uppercase) ||
      !CU_add_test(suite, "Starts With", tus_startsWith)) {
    CU_cleanup_registry();
    printf("Error: %i\n", CU_get_error());
  }
}


// ************************************************************************** //
// TEST_UTIL_NUMBER                                                           //
// ************************************************************************** //
void testUtilNumber() {
  // Adds the suite to general tests suite.                                   //
  CU_pSuite suite = CU_add_suite("Number Utils Suite", initUtilNumber, NULL);
  if (!suite ||
      !CU_add_test(suite, "Array to integer", tun_arrayToInteger) ||
      !CU_add_test(suite, "Is Integer", tun_isInteger) ||
      !CU_add_test(suite, "Boolean to String", tun_btos) ||
      !CU_add_test(suite, "Integer to String", tun_itos) ||
      !CU_add_test(suite, "Long Integer to String", tun_ltos) ||
      !CU_add_test(suite, "Unsigned Integer to String", tun_utos) ||
      !CU_add_test(suite, "Double to String", tun_dtos) ||
      !CU_add_test(suite, "Octal to String", tun_octal)) {
    CU_cleanup_registry();
    printf("Error: %i\n", CU_get_error());
  }
}


// ************************************************************************** //
// TEST_UTIL_FILE                                                             //
// ************************************************************************** //
void testUtilFile() {
  // Adds the suite to general tests suite.                                   //
  CU_pSuite suite = CU_add_suite("File Utils Suite", initUtilFile,
                                 cleanUtilFile);
  if (!suite ||
      !CU_add_test(suite, "File exists", tuf_fexists) ||
      !CU_add_test(suite, "File size", tuf_fsize)) {
    CU_cleanup_registry();
    printf("Error: %i\n", CU_get_error());
  }
}


// ************************************************************************** //
// TEST_UTIL_NETWORK                                                          //
// ************************************************************************** //
void testUtilNetwork() {
  // Adds the suite to general tests suite.                                   //
  CU_pSuite suite = CU_add_suite("Network Utils Suite", initUtilNetwork, NULL);
  if (!suite ||
      !CU_add_test(suite, "Array to Hexadecimal", tun_arrayToHex)) {
    CU_cleanup_registry();
    printf("Error: %i\n", CU_get_error());
  }
}


// ************************************************************************** //
// TEST_UTIL_SYSTEM                                                           //
// ************************************************************************** //
void testUtilSystem() {
  // Adds the suite to general tests suite.                                   //
  CU_pSuite suite = CU_add_suite("Stystem Utils Suite", initUtilNetwork, NULL);
  if (!suite ||
      !CU_add_test(suite, "Sleep thread", tusy_ssleep) ||
      !CU_add_test(suite, "Save PID", tusy_savePid)) {
    CU_cleanup_registry();
    printf("Error: %i\n", CU_get_error());
  }
}
