/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_TEST_SUITE_UTIL_STRING_H_
#define SRC_TEST_SUITE_UTIL_STRING_H_


//!
//! Initializes the util string suite.
//! @return 0 on success, non-zero otherwise.
//!
int initUtilString();

//!
//! Tests for trim function.
//!
void tus_trim();

//!
//! Tests for explode function.
//!
void tus_split();

//!
//! Tests for replace function.
//!
void tus_replace();

//!
//! Tests for indexOf function.
//!
void tus_indexOf();

//!
//! Tests for String to Map, and Map to String functions.
//!
void tus_stm();

//!
//! Tests for lowercase function.
//!
void tus_lowercase();

//!
//! Tests for uppercase function.
//!
void tus_uppercase();

//!
//! Tests for startsWith and startsWithCase functions.
//!
void tus_startsWith();


#endif  // SRC_TEST_SUITE_UTIL_STRING_H_
