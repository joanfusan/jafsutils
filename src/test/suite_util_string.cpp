/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./suite_util_string.h"

#include <CUnit/CUnit.h>
#include <string>

#include "../utils/util_string.h"

using std::string;


// Constants.                                                                 //
//! Full text of text for trim strings.                                       //
static const char TRIM_TEXT[] = "   ex  am   ple   ";
//! Example of text for left trim.                                            //
static const char TRIM_LEFT[] = "ex  am   ple   ";
//! Example of text for right trim.                                           //
static const char TRIM_RIGHT[] = "   ex  am   ple";
//! Example of text for trim.                                                 //
static const std::string TRIM_BOTH = "ex  am   ple";
//! Example text for replace characters 1.                                    //
static const std::string REPLACE_TEXT_1 = "___ex__am___ple___";
//! Example text for replace characters 2.                                    //
static const std::string REPLACE_TEXT_2 = "ax  am   pla";

//! String to map command.                                                    //
static const std::string STM_COMMAND = "command";
//! String to map value.                                                      //
static const std::string STM_VALUE = "value";
//! String to map other.                                                      //
static const std::string STM_OTHER = "other";
//! String to map result.                                                     //
static const std::string STM_RESULT = "result";
//! Example of string to map without value.                                   //
static const std::string STM_0 = " " + STM_COMMAND + "    =   ";
//! Example of string to map with 1 line.                                     //
static const std::string STM_1 = "  " + STM_COMMAND + "    =   " +
                                           STM_VALUE + "  ";
//! Example of string to map with 2 lines.                                    //
static const std::string STM_2 = STM_1 + "\n  " + STM_OTHER + "    =   " +
                                 STM_RESULT + "  ";


// ************************************************************************** //
// INIT_UTIL_STRING                                                           //
// ************************************************************************** //
int initUtilString() {
  return 0;
}


// ************************************************************************** //
// TUS_TRIM                                                                   //
// ************************************************************************** //
void tus_trim() {
  string data;
  string result = util::trim(data);
  CU_ASSERT_TRUE(result.empty());

  data = TRIM_TEXT;
  result = util::trim(data);
  CU_ASSERT_STRING_EQUAL(result.c_str(), TRIM_BOTH.c_str());

  data = TRIM_LEFT;
  result = util::trim(data);
  CU_ASSERT_STRING_EQUAL(result.c_str(), TRIM_BOTH.c_str());

  data = TRIM_RIGHT;
  result = util::trim(data);
  CU_ASSERT_STRING_EQUAL(result.c_str(), TRIM_BOTH.c_str());

  data = util::trim(result);
  CU_ASSERT_STRING_EQUAL(data.c_str(), result.c_str());
}


// ************************************************************************** //
// TUS_SPLIT                                                                  //
// ************************************************************************** //
void tus_split() {
  lstring results;
  string data;

  util::split(data, ' ', results);
  CU_ASSERT_TRUE(results.empty());

  data = TRIM_TEXT;
  util::split(data, ' ', results);
  CU_ASSERT_EQUAL_FATAL(results.size(), 3);

  lstring::iterator it = results.begin();
  CU_ASSERT_STRING_EQUAL(it->c_str(), "ex");
  ++it;
  CU_ASSERT_STRING_EQUAL(it->c_str(), "am");
  ++it;
  CU_ASSERT_STRING_EQUAL(it->c_str(), "ple");

  data = TRIM_TEXT;
  util::split(data, 'e', results);
  CU_ASSERT_EQUAL_FATAL(results.size(), 3);

  it = results.begin();
  CU_ASSERT_STRING_EQUAL(it->c_str(), "   ");
  ++it;
  CU_ASSERT_STRING_EQUAL(it->c_str(), "x  am   pl");
  ++it;
  CU_ASSERT_STRING_EQUAL(it->c_str(), "   ");

  data = TRIM_BOTH;
  util::split(data, 'z', results);
  CU_ASSERT_EQUAL_FATAL(results.size(), 1);

  it = results.begin();
  CU_ASSERT_STRING_EQUAL(it->c_str(), TRIM_BOTH.c_str());
}


// ************************************************************************** //
// TUS_REPLACE                                                                //
// ************************************************************************** //
void tus_replace() {
  string data;
  util::replace(data, ' ', 'a');
  CU_ASSERT_TRUE(data.empty());

  data = TRIM_TEXT;
  util::replace(data, ' ', ' ');
  CU_ASSERT_STRING_EQUAL(data.c_str(), TRIM_TEXT);

  data = TRIM_TEXT;
  util::replace(data, ' ', '_');
  CU_ASSERT_STRING_EQUAL(data.c_str(), REPLACE_TEXT_1.c_str());

  data = TRIM_BOTH;
  util::replace(data, 'e', 'a');
  CU_ASSERT_STRING_EQUAL(data.c_str(), REPLACE_TEXT_2.c_str());
}


// ************************************************************************** //
// TUS_INDEX_OF                                                               //
// ************************************************************************** //
void tus_indexOf() {
  CU_ASSERT_EQUAL(util::indexOf(TRIM_TEXT, "ex"), 3);
  CU_ASSERT_EQUAL(util::indexOf(TRIM_TEXT, "e   "), 14);
  CU_ASSERT_EQUAL(util::indexOf(TRIM_TEXT, "m"), 8);
  CU_ASSERT_EQUAL(util::indexOf(TRIM_TEXT, "no"), -1);
  CU_ASSERT_EQUAL(util::indexOf("", "no"), -1);
  CU_ASSERT_EQUAL(util::indexOf(TRIM_TEXT, ""), -1);
  CU_ASSERT_EQUAL(util::indexOf("", ""), -1);
}


// ************************************************************************** //
// TUS_STM                                                                    //
// ************************************************************************** //
void tus_stm() {
  string data;
  mstring result;

  util::stom(data, result);
  CU_ASSERT(result.empty());

  data = STM_COMMAND;
  util::stom(data, result);
  CU_ASSERT(result.empty());

  data = STM_0;
  util::stom(data, result);
  CU_ASSERT_FALSE(result.empty());
  CU_ASSERT_NOT_EQUAL(result.find(STM_COMMAND), result.end());
  CU_ASSERT(result[STM_COMMAND].empty());

  data = STM_1;
  util::stom(data, result);
  CU_ASSERT_FALSE(result.empty());
  CU_ASSERT_NOT_EQUAL(result.find(STM_COMMAND), result.end());
  CU_ASSERT_EQUAL(result[STM_COMMAND], STM_VALUE);

  data = STM_2;
  util::stom(data, result);
  CU_ASSERT_FALSE(result.empty());
  CU_ASSERT_EQUAL(result.size(), 2);
  CU_ASSERT_NOT_EQUAL(result.find(STM_COMMAND), result.end());
  CU_ASSERT_NOT_EQUAL(result.find(STM_OTHER), result.end());
  CU_ASSERT_EQUAL(result[STM_COMMAND], STM_VALUE);
  CU_ASSERT_EQUAL(result[STM_OTHER], STM_RESULT);
}


// ************************************************************************** //
// TUS_LOWERCASE                                                              //
// ************************************************************************** //
void tus_lowercase() {
  string data;
  CU_ASSERT(util::lowercase(data).empty());

  data = "UppErlOwEr";
  CU_ASSERT_STRING_EQUAL(util::lowercase(data).c_str(), "upperlower");

  data = "lowercase";
  CU_ASSERT_STRING_EQUAL(util::lowercase(data).c_str(), data.c_str());
}


// ************************************************************************** //
// TUS_UPPERCASE                                                              //
// ************************************************************************** //
void tus_uppercase() {
  string data;
  CU_ASSERT(util::uppercase(data).empty());

  data = "UppErlOwEr";
  CU_ASSERT_STRING_EQUAL(util::uppercase(data).c_str(), "UPPERLOWER");

  data = "UPPERCASE";
  CU_ASSERT_STRING_EQUAL(util::uppercase(data).c_str(), data.c_str());
}


// ************************************************************************** //
// TUS_STARTS_WITH                                                            //
// ************************************************************************** //
void tus_startsWith() {
  string data;
  string prefix;

  CU_ASSERT(util::startsWith(data, prefix));
  CU_ASSERT(util::startsWithCase(data, prefix));

  data = "a";
  CU_ASSERT(util::startsWith(data, prefix));
  CU_ASSERT(util::startsWithCase(data, prefix));

  data = "";
  prefix = "a";
  CU_ASSERT_FALSE(util::startsWith(data, prefix));
  CU_ASSERT_FALSE(util::startsWithCase(data, prefix));

  data = "house";
  CU_ASSERT_FALSE(util::startsWith(data, prefix));
  CU_ASSERT_FALSE(util::startsWithCase(data, prefix));

  prefix = "hou";
  CU_ASSERT(util::startsWith(data, prefix));
  CU_ASSERT(util::startsWithCase(data, prefix));

  prefix = "HoUsE";
  CU_ASSERT_FALSE(util::startsWith(data, prefix));
  CU_ASSERT(util::startsWithCase(data, prefix));

  data = "House is red";
  CU_ASSERT_FALSE(util::startsWith(data, prefix));
  CU_ASSERT(util::startsWithCase(data, prefix));

  prefix = "House";
  CU_ASSERT(util::startsWith(data, prefix));
  CU_ASSERT(util::startsWithCase(data, prefix));
}
