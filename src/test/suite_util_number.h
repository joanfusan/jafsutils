/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_TEST_SUITE_UTIL_NUMBER_H_
#define SRC_TEST_SUITE_UTIL_NUMBER_H_


//!
//! Initializes the util number suite.
//! @return 0 on success, non-zero otherwise.
//!
int initUtilNumber();

//!
//! Tests for arrayToInteger function.
//!
void tun_arrayToInteger();

//!
//! Tests for isInteger function.
//!
void tun_isInteger();

//!
//! Tests for btos function.
//!
void tun_btos();

//!
//! Tests for itos function.
//!
void tun_itos();

//!
//! Tests for ltos function.
//!
void tun_ltos();

//!
//! Tests for utos function.
//!
void tun_utos();

//!
//! Tests for dtos function.
//!
void tun_dtos();

//!
//! Tests for octal function.
//!
void tun_octal();

#endif  // SRC_TEST_SUITE_UTIL_NUMBER_H_
