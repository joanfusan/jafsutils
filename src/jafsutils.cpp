/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./jafsutils.h"

#include <stdio.h>
#include <string.h>
#include <string>

//! Name of utils library.                                                    //
#define LIBRARY_NAME     "jafsutils"
//! Utils library current version.                                            //
#define LIBRARY_VERSION  "1.0"


// ************************************************************************** //
// UTILS_LIBRARY_VERSION                                                      //
// ************************************************************************** //
string utils_library_version() {
  char version[30];
  memset(version, 0, sizeof(version));
  snprintf(version, sizeof(version), "%s %s\n", LIBRARY_NAME, LIBRARY_VERSION);

  return version;
}
