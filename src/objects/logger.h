/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_OBJECTS_LOGGER_H_
#define SRC_OBJECTS_LOGGER_H_

#include <string>

using std::string;

//! Macro that includes file name and the line to the log.
#define LFL __FILE__, __LINE__


//!
//! @brief   Creates a log file where store lines of debug.
//! @author  Jose Antonio Fuentes Santiago
//! @version 1.0
//!
class Logger {
  public:
    //!
    //! @brief  Enumeration with log levels.
    //!
    typedef enum {
      //! Debug log level.
      DEBUG,
      //! Info log level.
      INFO,
      //! Warning log level.
      WARNING,
      //! Error log level.
      ERROR
    } LogLevel;

  private:
    //! String with date format to generate the name of log file.
    static const char DATE_FORMAT[];

    //! Pointer to the unique log instance.
    static Logger * instance;
    //! Path to the directory log.
    string directory;
    //! Value with log status.
    bool enabled;
    //! Log level.
    LogLevel level;


    //!
    //! @brief  Default constructor for the class. Enable the log and set level
    //!         to debug.
    //!
    Logger();

    //!
    //! @brief  Create the main directory of the log.
    //!
    void createDirs();

    //!
    //! @brief  Adds a log message to the logger registry.
    //! @param  type      Log level of the message.
    //! @param  message   String with the message to store.
    //! @param  filename  Filename to add in the log line.
    //! @param  line      Line to add in the log line.
    //!
    void add(const LogLevel type, const string & message,
             const string & filename = "", const int line = 0);

  public:
    /**
     * @brief  Gets an instance of the class.
     * @return Logger class instance.
     */
    static Logger * getInstance();

    //!
    //! @brief  Prints new line with date.
    //!
    void start();

    //!
    //! @brief  Ads an error message into log.
    //! @param  message   Message to save into the log.
    //! @param  filename  Filename that calls this log.
    //! @param  line      Line from calls this log.
    //!
    void error(const string & message, const string & filename = "",
               const int line = 0);

    //!
    //! @brief  Ads a warning message into log.
    //! @param  message   Message to save into the log.
    //! @param  filename  Filename that calls this log.
    //! @param  line      Line from calls this log.
    //!
    void warning(const string & message, const string & filename = "",
                 const int line = 0);

    //!
    //! @brief  Ads a information message into log.
    //! @param  message   Message to save into the log.
    //! @param  filename  Filename that calls this log.
    //! @param  line      Line from calls this log.
    //!
    void info(const string & message, const string & filename = "",
              const int line = 0);

    //!
    //! @brief  Ads a debug message into log.
    //! @param  message   Message to save into the log.
    //! @param  filename  Filename that calls this log.
    //! @param  line      Line from calls this log.
    //!
    void debug(const string & message, const string & filename = "",
               const int line = 0);

    //!
    //! @brief  Sets if log is active or inactive.
    //! @param  enabled  Boolean value with status of log.
    //!
    void setEnabled(const bool enabled) { this->enabled = enabled; }

    //!
    //! @brief  Gets a boolean value with log status.
    //! @return boolean value with status of log.
    //!
    bool isEnabled() const { return enabled; }

    //!
    //! @brief  Sets the minimun level for log.
    //! @param  level  Minimun level for log.
    //!
    void setLevel(const LogLevel level) { this->level = level; }

    //!
    //! @brief  Gets the log level.
    //! @return Value with log level.
    //!
    LogLevel getLevel() const { return level; }

    //!
    //! @brief  Sets the directory where store the log.
    //! @param  directory  Path to the directory where store the log.
    //!
    void setDirectory(const string & directory) { this->directory = directory; }

    //!
    //! @brief  Gets the path for log directory.
    //! @return String with path for log directory.
    //!
    string getDirectory() const { return directory; }
};

#endif  // SRC_OBJECTS_LOGGER_H_
