/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./logger.h"
#include "../jafsutils.h"

#ifdef JAFS_WIN
#include <io.h>
#endif
#include <stdio.h>
#include <sys/stat.h>
#include "./date.h"
#include "../utils/util_file.h"
#include "../utils/util_number.h"

// Static variables and constants definition.                                 //
const char Logger::DATE_FORMAT[] = "%Y%m%d";
Logger * Logger::instance = NULL;


// ************************************************************************** //
// LOGGER                                                                     //
// ************************************************************************** //
Logger::Logger() : enabled(true), level(DEBUG) {;
}


// ************************************************************************** //
// ADD                                                                        //
// ************************************************************************** //
void Logger::add(const LogLevel type, const string & message,
                 const string & filename, const int line) {
  if (enabled && type >= this->level) {
    Date date;

    /* Verificamos si ya existe el directorio donde almacenar el registro, en */
    /* caso de no existir lo creamos.                                         */
    /* ---------------------------------------------------------------------- */
    createDirs();

    /* Composición del nombre del fichero y apertura del mismo en modo de     */
    /* sobrescritura.                                                         */
    string logfile = directory + "/" + date.toString(Logger::DATE_FORMAT) +
                     ".txt";

    // Opens the file where store the log message.                            //
    FILE * file = fopen(logfile.c_str(), "a");
    if (file) {
      string text = date.toString() + " - ";

      // Gets the string with log level received.                             //
      if (type == DEBUG) {
        text += "DEBUG  ";
      } else if (type == INFO) {
        text += "INFO   ";
      } else if (type == WARNING) {
        text += "WARNING";
      } else {
        text += "ERROR  ";
      }

      text += ": ";

      // Adds the filename and line if received.                              //
      if (!filename.empty()) {
#ifdef JAFS_LINUX
        text += filename.substr(filename.find_last_of('/') + 1);
#else
        text += filename.substr(filename.find_last_of('\\') + 1);
#endif

        if (line) {
          text += ":" + util::itos(line);
        }
        text += "::";
      }

      text += message + "\n";

      fputs(text.c_str(), file);

      fclose(file);
    }
  }
}


/* ************************************************************************** */
/* ERROR                                                                      */
/* ************************************************************************** */
void Logger::error(const string & message, const string & filename,
                   const int line) {
  add(ERROR, message, filename, line);
}


// ************************************************************************** //
// WARNING                                                                    //
// ************************************************************************** //
void Logger::warning(const string & message, const string & filename,
                     const int line) {
  add(WARNING, message, filename, line);
}


// ************************************************************************** //
// INFO                                                                       //
// ************************************************************************** //
void Logger::info(const string & message, const string & filename,
                  const int line) {
  add(INFO, message, filename, line);
}


/* ************************************************************************** */
/* DEBUG                                                                      */
/* ************************************************************************** */
void Logger::debug(const string & message, const string & filename,
                   const int line) {
  add(DEBUG, message, filename, line);
}


// ************************************************************************** //
// START                                                                      //
// ************************************************************************** //
void Logger::start() {
  debug("");;
  debug("---------- " + Date().toString() + " ----------");
  debug("");
}


// ************************************************************************** //
// GET_INSTANCE                                                               //
// ************************************************************************** //
Logger * Logger::getInstance() {
  if (!instance) {
    instance = new Logger();
  }

  return instance;
}


// ************************************************************************** //
// CREATE_DIRS                                                                //
// ************************************************************************** //
void Logger::createDirs() {
  if (!directory.empty()) {
    // Creates the parent directory if no exists.                             //
    struct stat esStat;
    const size_t position = directory.find_last_of('/');

    if (position != directory.npos && position > 1) {
      string topdir = directory.substr(0, position);

      if (stat(topdir.c_str(), &esStat)) {
#ifdef JAFS_LINUX
        mkdir(topdir.c_str(), 0777);
#else
        mkdir(topdir.c_str());
#endif
      }
    }

    // Creates the main directory.                                            //
    if (stat(directory.c_str(), &esStat)) {
#ifdef JAFS_LINUX
      mkdir(directory.c_str(), 0777);
#else
      mkdir(directory.c_str());
#endif
    }
  }
}
