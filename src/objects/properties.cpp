/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./properties.h"

#include <stdio.h>
#include <string.h>
#include <string>

#include "../utils/util_string.h"

using std::string;


// ************************************************************************** //
// LOAD_FILE                                                                  //
// ************************************************************************** //
bool Properties::loadFile(const string & filename) {
  clean();

  this->filename = filename;

  return load();
}


// ************************************************************************** //
// TO_STRING                                                                  //
// ************************************************************************** //
string Properties::toString() {
  string result;                                         // String to return. //

  // Iterates over the properties map and append to string the keys with its  //
  // asociated values.                                                        //
  for (mstring::iterator it = config.begin(); it != config.end(); ++it) {
    result += it->first + util::PARAMS_SEPARATOR + it->second +
              util::VALUES_SEPARATOR;
  }

  return result;
}


// ************************************************************************** //
// CLEAN                                                                      //
// ************************************************************************** //
void Properties::clean() {
  if (!config.empty()) {
    config.clear();
  }
}


// ************************************************************************** //
// LOAD                                                                       //
// ************************************************************************** //
bool Properties::load() {
  FILE * file = fopen(filename.c_str(), "r");

  if (file) {
    char readed[256];                     // Current string readed from file. //

    // Read the file content.                                                 //
    while (!feof(file)) {
      // Reads a line from the file and stores into a string variable.        //
      memset(readed, 0, sizeof(readed));
      if (fgets(readed, sizeof(readed), file)) {
        string current = readed;

        // Deletes new line and end of file.                                  //
        if (!current.empty() && (CHAR_NEWLINE == current[current.size() - 1] ||
                                 EOF == current[current.size() - 1])) {
          current = current.substr(0, current.size() - 1);
        }

        // Analyzes that the current line is valid.                           //
        if (!current.empty() && CHAR_COMMENT != current[0]) {
          // Gets the equals sign position.                                   //
          size_t position = current.find(CHAR_EQUALS);

          // Checks if the equals character is found..                        //
          if (current.npos != position) {
            // Gets the key of the property.                                  //
            string key = current.substr(0, position);
            util::trim(key);

            // Gets the value of the property.                                //
            string value = current.substr(position + 1);
            util::trim(value);

            // If the key and value are valid, will be stored in the map.     //
            if (!key.empty() && !value.empty()) {
              config[key] = value;
            }
          }
        }
      }

      fclose(file);
    }
  }

  return !config.empty();
}
