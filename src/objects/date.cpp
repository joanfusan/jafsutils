/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./date.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

using std::string;


// Static variables and constants definition.                                 //
const char Date::DEFAULT_FORMAT[] = "%Y-%m-%d %H:%M:%S";


// ************************************************************************** //
// DATE                                                                       //
// ************************************************************************** //
Date::Date(const string & instant) {
  if (!instant.empty()) {
    struct tm st_tm;                // Struct that stores received date.      //
    time_t st_t;                    // Stores seconds number from 1.970.      //
    string aux;                     // Auxiliar string for use in operations. //

    // Gets the date without miliseconds.                                     //
    aux = instant.substr(0, instant.size() - 4);

    // TODO(joanfusan) define for Windows.
#ifdef JAFS_LINUX
    // Verifies if date is valid and gets the date.                           //
    if (strptime(instant.c_str(), DEFAULT_FORMAT, &st_tm)) {
      // Gets the time struct.                                                //
      st_tm.tm_isdst = -1;
      st_t = mktime(&st_tm);

      // Gets miliseconds number to store.                                    //
      aux = instant.substr(instant.size() - 3);

      // Updates instant structure.                                           //
      timestamp.tv_sec = st_t;
      timestamp.tv_usec = atoi(aux.c_str()) * 1000;        // To microseconds //
    }
#else
  // TODO implement Windows version
#endif
  }
}


// ************************************************************************** //
// DATE                                                                       //
// ************************************************************************** //
Date::Date(const double & instant) {
  if (instant >= 0) {
    uint64_t seconds = (uint64_t) floor(instant / 1000.0);
    uint64_t micros = (uint64_t) round(fabs(instant - (seconds * 1000.0)) *
                                       1000.0);

    // Save the time received.                                                //
    timestamp.tv_sec = seconds;
    timestamp.tv_usec = micros;
  }
}


// ************************************************************************** //
// GET_MILISENCONDS                                                           //
// ************************************************************************** //
uint64_t Date::getMiliseconds() const {
  return ((uint64_t) this->timestamp.tv_sec * 1000 +
          (uint64_t) this->timestamp.tv_usec / 1000);
}


// ************************************************************************** //
// GET_TIMESTAMP                                                              //
// ************************************************************************** //
double Date::getTimestamp() {
  return static_cast<double> (getSeconds() * 1000.0) +
         static_cast<double> (getMicroseconds() / 1000000.0);
}


// ************************************************************************** //
// TO_STRING                                                                  //
// ************************************************************************** //
string Date::toString() {
  return toString(DEFAULT_FORMAT);
}


// ************************************************************************** //
// TO_STRING                                                                  //
// ************************************************************************** //
string Date::toString(const string & format) {
  // Gets the time struct corresponding to current seconds value.             //
#ifdef JAFS_LINUX
  struct tm * time = localtime(&timestamp.tv_sec);
#else
  struct tm * time = localtime((time_t *) &timestamp.tv_sec);
#endif


  char dateMicro[80];                         // String with result date.     //
  char dateSeconds[70];                       // String with date in seconds. //
  memset(dateMicro, 0, sizeof(dateMicro));
  memset(dateSeconds, 0, sizeof(dateSeconds));

  // Gets the miliseconds, and concatenate to date string.                    //
  strftime(dateSeconds, sizeof(dateSeconds), format.c_str(), time);
  uint64_t milis = timestamp.tv_usec / 1000;

#ifdef JAFS_64
  snprintf(dateMicro, sizeof(dateMicro), "%s.%03lu", dateSeconds, milis);

#elif defined (_WIN32)
  snprintf(dateMicro, sizeof(dateMicro), "%s.%I64d", dateSeconds, milis);
#else
  snprintf(dateMicro, sizeof(dateMicro), "%s.%03llu", dateSeconds, milis);
#endif

  return dateMicro;
}


// ************************************************************************** //
// OPERATOR_-                                                                 //
// ************************************************************************** //
double Date::operator-(const Date & date) {
  // Gets the seconds and microseconds in decimal form.                       //
  double value1 = static_cast<double> (date.getSeconds()) +
                  static_cast<double> (date.getMicroseconds() / 1000000.0);
  double value2 = static_cast<double> (timestamp.tv_sec) +
                  static_cast<double> (timestamp.tv_usec / 1000000.0);

  return fabs(value1 - value2);
}


// ************************************************************************** //
// STMS                                                                       //
// ************************************************************************** //
uint64_t Date::stms(const int seconds) {
  uint64_t milis = 0;

  if (seconds > 0) {
    milis = seconds * 1000;
    if (milis < 0) {
      milis = 0;
    }
  }

  return milis;
}
