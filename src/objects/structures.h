/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_OBJECTS_STRUCTURES_H_
#define SRC_OBJECTS_STRUCTURES_H_

#include <map>
#include <list>
#include <string>

using std::map;
using std::list;
using std::string;

//! Data type that stores a map with key and value of string type.
typedef map<string, string> mstring;
//! Data type that stores a list with values of string type.
typedef list<string> lstring;

#endif  // SRC_OBJECTS_STRUCTURES_H_
