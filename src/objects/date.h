/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_OBJECTS_DATE_H_
#define SRC_OBJECTS_DATE_H_

#include <time.h>
#include <stdint.h>
#include <sys/time.h>
#include <string>

using std::string;

//!
//! @brief   Stores a date defined by seconds and microseconds.
//! @details Stores a timestamp defined by seconds and microseconds from
//!          January, 1st of 1.970.
//! @author  Jose Antonio Fuentes Santiago
//! @version 1.0
//!
class Date {
  private:
    //! Default date format without miliseconds.
    static const char DEFAULT_FORMAT[];

    //! Struct that stores the time.
    struct timeval timestamp;

  public:
    //! Number of seconds in an hour.
    static const uint32_t SECS_HOUR = 3600;
    //! Number of seconds in an minute.
    static const uint32_t SECS_MINUTE = 60;

    //!
    //! @brief  Returns miliseconds number that represents the seconds number
    //!         received.
    //! @param  seconds  Seconds number to convert.
    //! @return Miliseconds number result of conversión or 0 in case of negative
    //!         value.
    //!
    static uint64_t stms(const int seconds);

    //!
    //! @brief  Constructor that receives a string with time to set.
    //! @param  instant  String with instant to set.
    //! @todo   Add a format to instant.
    //!
    explicit Date(const string & instant);

    //!
    //! @brief  Constructor that receives a double value with time to set.
    //! @param  instant  Double with instant to set.
    //!
    explicit Date(const double & instant);

    //!
    //! @brief  Returns the miliseconds number in current timestamp.
    //! @return An integer value with miliseconds in current timestamp.
    //!
    uint64_t getMiliseconds() const;

    //!
    //! @brief  Returns the timestamp stored in class in a double value, in
    //!         which integer part is the seconds number, and decimal part is
    //!         the microseconds number.
    //! @return A double value with current timestamp.
    //!
    double getTimestamp();

    //!
    //! @brief  Returns a string with current date in standard format.
    //! @return String with current date in standard format.
    //!
    string toString();

    //!
    //! @brief  Returns a string with current date with received format.
    //! @param  format  String with format in date returned. See strptime()
    //!                 allowed params.
    //! @return String with current date in standard format.
    //! @see    strptime()
    //!
    string toString(const string & format);

    //!
    //! @brief  Substract to the current date, the received instant and gets the
    //!         diference between the two date.
    //! @param  date  Fecha a restar a la actual.
    //! @return Double value with seconds and microseconds of diference.
    //!
    double operator-(const Date & date);


    // ********************************************************************** //
    // INLINE FUNCTIONS                                                       //
    // ********************************************************************** //
    //!
    //! @brief  Returns number of miliseconds that contains received minutes.
    //! @param  minutes  Number of minutes to calculate.
    //! @return Number of miliseconds.
    //!
    static uint64_t mtms(const uint32_t minutes) {
      return stms(SECS_MINUTE * minutes);
    }

    //!
    //! @brief  Returns number of miliseconds that contains received hours.
    //! @param  hours  Number of hours to calculate.
    //! @return Number of miliseconds.
    //!
    static uint64_t htms(const uint32_t hours) {
      return stms(SECS_HOUR * hours);
    }

    //!
    //! @brief  Default constructor for class.
    //!
    Date() { update(); }

    //!
    //! @brief  Returns number of seconds in current timestamp.
    //! @return Number of seconds in current timestamp.
    //!
    uint64_t getSeconds() const { return timestamp.tv_sec; }

    //!
    //! @brief  Returns number of microseconds in current timestamp.
    //! @return Number of microseconds in current timestamp.
    //!
    uint64_t getMicroseconds() const { return timestamp.tv_usec; }

    //!
    //! @brief  Updates date to current timestamp.
    //!
    void update() { gettimeofday(&timestamp, NULL); }

    //!
    //! @brief  Overwrites the equals operator.
    //! @param  date  Date to assign.
    //! @return Current object instance.
    //!
    Date & operator=(Date date) { timestamp = date.timestamp; return *this; }
};

#endif  // SRC_OBJECTS_DATE_H_
