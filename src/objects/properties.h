/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_OBJECTS_PROPERTIES_H_
#define SRC_OBJECTS_PROPERTIES_H_

#include <stdlib.h>
#include <string>

#include "./structures.h"

using std::string;


//!
//! @brief   Class that reads a file with keys-values pairs, and allows to
//!          access to reader values.
//! @author  Jose Antonio Fuentes Santiago
//! @version 1.0
//!
class Properties {
  private:
    //! Characted used in comments.
    static const char CHAR_COMMENT = '#';
    //! Character used to separate keys from values.
    static const char CHAR_EQUALS = '=';
    //! Character that represents new line.
    static const char CHAR_NEWLINE = '\n';

    //! Map with keys and values.
    mstring config;
    //! Name of the current file.
    string filename;

    //!
    //! Removes all configuration values loaded.
    //!
    void clean();

    //!
    //! @brief  Load the current file values into memory.
    //! @return Boolean value that indicates if file load is ok.
    //!
    bool load();

  public:
    //!
    //! @brief  Default constructor for the class.
    //!
    Properties() { }

    //!
    //! @brief  Loads the file with received filename to access to data.
    //! @param  filename  Name of the file to load.
    //! @return true is file is loaded successfully, false in another case.
    //!
    bool loadFile(const string & filename);

    //!
    //! @brief  Returns a string with the loaded values.
    //! @return String with loaded values.
    //!
    string toString();


    // ********************************************************************** //
    // INLINE FUNCTIONS                                                       //
    // ********************************************************************** //

    //!
    //! @brief  Returns a value associated with received key, or empty string
    //!         if key not exists.
    //! @param  name  Key to search.
    //! @return String with value, or empty string if key not found.
    //!
    string getString(const string & name) { return config[name]; }

    //!
    //! @brief  Returns a double value associated with received key, or 0 if key
    //!         doesn't exists or value is not double type.
    //! @param  name  Key to search.
    //! @return Double with value, or 0 if key not found.
    //!
    double getDouble(const string & name) {
      return strtod(config[name].c_str(), NULL);
    }

    //!
    //! @brief  Returns a float value associated with received key, or 0 if key
    //!         doesn't exists or value is not float type.
    //! @param  name  Key to search.
    //! @return Float with value, or 0 if key not found.
    //!
    float getFloat(const string & name) { return atof(config[name].c_str()); }

    //!
    //! @brief  Returns an integer value associated with received key, or 0 if
    //!         key doesn't exists or value is not integer type.
    //! @param  name  Key to search.
    //! @return Integer with value, or 0 if key not found.
    //!
    int getInt(const string & name) { return atoi(config[name].c_str()); }

    //!
    //! @brief  Returns the current file name.
    //! @return String with current file name.
    //!
    string getFilename() const { return filename; }

    //!
    //! @brief  Returns a value that indicates if there is no properties loaded.
    //! @return Boolean value that indicates if there is no properties loaded.
    //!
    bool isEmpty() const { return config.empty(); }
};

#endif  // SRC_OBJECTS_PROPERTIES_H_
