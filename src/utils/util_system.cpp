/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./util_system.h"

#include <stdio.h>
#include <unistd.h>
#ifndef JAFS_LINUX
#include <windows.h>
#endif
#include <string>

using std::string;


// ************************************************************************** //
// SSLEEP                                                                     //
// ************************************************************************** //
void util::ssleep(const uint32_t time) {
#ifdef JAFS_LINUX
  usleep(time * 1000);
#else
  Sleep(time);
#endif
}


// ************************************************************************** //
// SAVE_PID                                                                   //
// ************************************************************************** //
void util::savePid(const string & filename) {
  if (!filename.empty()) {
    FILE * file = fopen(filename.c_str(), "w");

    if (file) {
      fprintf(file, "%i\n", getpid());
      fclose(file);
    }
  }
}


// ************************************************************************** //
// EXECUTE_COMMAND                                                            //
// ************************************************************************** //
string util::executeCommand(const string & command) {
  string result;

  FILE * pipe = popen(command.c_str(), "r");
  if (pipe) {
    char aux[512];
    while (fgets(aux, sizeof(aux), pipe)) {
      result += string(aux);
    }

    pclose(pipe);
  }

  return result;
}
