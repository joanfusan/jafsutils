/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./util_string.h"

#include <string.h>
#include <string>

using std::string;


// ************************************************************************** //
// MTOS                                                                       //
// ************************************************************************** //
string util::mtos(mstring & data) {
  string result = "";                                    // String to return. //

  for (mstring::iterator it = data.begin(); it != data.end(); ++it) {
    result += it->first + VALUES_SEPARATOR + it->second + PARAMS_SEPARATOR;
  }

  return result;
}


// ************************************************************************** //
// STOM                                                                       //
// ************************************************************************** //
void util::stom(const std::string & data, mstring & result) {
  if (!data.empty()) {
    lstring keys;                                         // Key list reader. //

    // Cleans the result map.                                                 //
    if (!result.empty()) {
      result.clear();
    }

    split(data, PARAMS_SEPARATOR, keys);

    // Recorre la lista de cadenas y obtiene la configuración.                //
    for (lstring::iterator it = keys.begin(); it != keys.end(); ++it) {
      string current = *it;
      size_t position = current.find_first_of(VALUES_SEPARATOR);

      if (string::npos != position) {
        string param = current.substr(0, position);
        string value = current.substr(position + 1);

        param = trim(param);
        value = trim(value);

        result[param] = value;
      }
    }
  }
}


// ************************************************************************** //
// STRTRIM                                                                    //
// ************************************************************************** //
string util::trim(const string & data) {
  string result;

  if (!data.empty()) {
    // Deletes black spaces in the left side.                                 //
    size_t i = 0;
    for (i = 0; i < data.length(); ++i) {
      if (' ' != data[i]) {
        result = data.substr(i);
        break;
      }
    }

    // Deletes black spaces in the right side.                                //
    for (i = result.length() - 1; i >= 0; --i) {
      if (' ' != result[i]) {
        result = result.substr(0, i + 1);
        break;
      }
    }
  }

  return result;
}


// ************************************************************************** //
// EXPLODE                                                                    //
// ************************************************************************** //
void util::split(const string & data, char separator, lstring & result) {
  size_t position = 0;                           // Current position.         //
  string current = data;                         // String used in iteration. //

  // Cleaning of received list.                                               //
  if (!result.empty()) {
    result.clear();
  }

  // Iterate over the string, searching de split character, if find it gets   //
  // the substring with value.                                                //
  while ((position = current.find_first_of(separator)) != string::npos) {
    string value = current.substr(0, position);
    if (!value.empty()) {
      result.push_back(value);
    }

    current = current.substr(position + 1);
  }

  // Adds the last string into the list, if exists.                           //
  if (!current.empty()) {
    result.push_back(current);
  }
}


// ************************************************************************** //
// REPLACE                                                                    //
// ************************************************************************** //
void util::replace(string & data, const char oldChar, const char newChar) {
  for (size_t i = 0; i < data.size(); ++i) {
    if (oldChar == data[i]) {
      data[i] = newChar;
    }
  }
}


// ************************************************************************** //
// INDEX_OF                                                                   //
// ************************************************************************** //
int util::indexOf(const string & data, const string & substring) {
  int position = -1;

  if (!data.empty() && !substring.empty()) {
    const char * result = strstr(data.c_str(), substring.c_str());
    if (result) {
      position = result - data.c_str();
    }
  }

  return position;
}


// ************************************************************************** //
// LOWERCASE                                                                  //
// ************************************************************************** //
string util::lowercase(const string & data) {
  string result;

  for (size_t i = 0; i < data.size(); ++i) {
    result += tolower(data[i]);
  }

  return result;
}


// ************************************************************************** //
// UPPERCASE                                                                  //
// ************************************************************************** //
string util::uppercase(const string & data) {
  string result;

  for (size_t i = 0; i < data.size(); ++i) {
    result += toupper(data[i]);
  }

  return result;
}


// ************************************************************************** //
// STARTS_WITH                                                                //
// ************************************************************************** //
bool util::startsWith(const string & data, const string & prefix) {
  bool starts = false;

  if ((data.empty() && prefix.empty()) || (data == prefix)) {
    starts = true;
  } else {
    size_t size = prefix.length();

    if (size < data.length()) {
      starts = (data.substr(0, size) == prefix);
    }
  }

  return starts;
}


// ************************************************************************** //
// STARTS_WITH_CASE                                                           //
// ************************************************************************** //
bool util::startsWithCase(const string & data, const string & prefix) {
  return startsWith(lowercase(data), lowercase(prefix));
}
