/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_UTILS_UTIL_NETWORK_H_
#define SRC_UTILS_UTIL_NETWORK_H_

#include <stdint.h>
#include <list>
#include <string>

using std::list;
using std::string;


//!
//! @brief   Namespace with a set of jafstutils functions.
//! @author  Jose Antonio Fuentes Santiago
//! @version 1.0
//!
namespace util {
  //!
  //! @brief   Defines a network interface.
  //! @author  Jose Antonio Fuentes Santiago
  //! @version 1.0
  //!
  typedef struct {
    //! Interface index number.
    uint32_t index;
    //! Interface name.
    string name;
    //! IP address of the interface.
    string ip;
    //! MAC address of the interface.
    string mac;
    //! Network mask of the interface.
    string netmask;
    //! IP address of gateway of interface.
    string gateway;
  } net_iface;

  //! Defines a list of interfaces.
  typedef list<net_iface> linterfaces;

  //!
  //! Gets an hexadecimal string with content of received array.
  //! @param  data   Array of char with content to extract.
  //! @param  end    Indicates the end position in array.
  //! @param  start  Indicates start position in array.
  //! @return String with hexadecimal content of received array.
  //!
  string arrayToHex(const char * data, const size_t start, const size_t end);

  //!
  //! Gets the first IP address finded.
  //! @return  String with first IP address.
  //! @todo    Receive interface name.
  //! @todo    Function with IPv6.
  //!
  uint32_t getIp();

  //!
  //! Returns a boolean value that indicates if received IP address is valid.
  //! @param  ip  String IP address to check.
  //! @return Boolean value that indicates if received IP address is valid.
  //! @todo   Function with IPv6.
  //!
  bool isValidIp(const string & ip);

#ifdef JAFS_LINUX
  //!
  //! Gets a list with current system interfaces.
  //! @param  interfaces  List of interfaces in the system.
  //!
  void getInterfaces(linterfaces & interfaces);
#endif

  //!
  //! Returns a MAC address from received array.
  //! @param  data  Array from extract MAC address.
  //! @param  size  Size of received array.
  //! @return MAC address extracted.
  //!
  string getMac(const char * data, const size_t size);
}  // namespace util

#endif  // SRC_UTILS_UTIL_NETWORK_H_
