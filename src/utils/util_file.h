/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_UTILS_UTIL_FILE_H_
#define SRC_UTILS_UTIL_FILE_H_

#include <stdint.h>
#include <string>

using std::string;

//!
//! @brief   Namespace with a set of jafstutils functions.
//! @author  Jose Antonio Fuentes Santiago
//! @version 1.0
//!
namespace util {
  //!
  //! Returns a value that indicated if file or directory, with received file
  //! name received exits.
  //! @param  filename  Name of the file or directory to check.
  //! @return true if the file or directory denoted by this pathname exists,
  //!         false otherwise.
  //!
  bool fexists(const string & filename);

  //!
  //! Gets the received file size.
  //! @param  filename  Name of the file to check.
  //! @param  size      Size returned of the file.
  //! @return Boolean value that will be true when the file size can be obtained
  //!         successfully, false otherwise.
  //!
  bool fsize(const string & filename, size_t & size);


  //!
  //! Make a directory and parent directories as needed. The directory created
  //! will have the specified mode.
  //! @param  path  Path to the directory to make.
  //! @param  mode  Set permission mode for directory (optional).
  //! @return Boolean value that will be true when path creation is ok, false
  //!         otherwise.
  //!
  bool makedir(const string & path, const uint32_t mode = 0xFFFFFFFF);
}  // namespace util

#endif  // SRC_UTILS_UTIL_FILE_H_
