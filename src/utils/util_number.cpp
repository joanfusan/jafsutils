/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./util_number.h"

#include <inttypes.h>
#include <stdio.h>
#include <string>

using std::string;


// ************************************************************************** //
// BTOS                                                                       //
// ************************************************************************** //
string util::btos(const bool value) {
  return value ? "true": "false";
}

// ************************************************************************** //
// ITOS                                                                       //
// ************************************************************************** //
string util::itos(const int32_t value) {
  char tempArray[12];
  snprintf(tempArray, sizeof(tempArray), "%i", value);

  return tempArray;
}


// ************************************************************************** //
// LTOS                                                                       //
// ************************************************************************** //
string util::ltos(const int64_t & value) {
  char tempArray[14];

#ifdef JAFS_64
  snprintf(tempArray, sizeof(tempArray), "%li", value);
#elif defined (_WIN32)
  snprintf(tempArray, sizeof(tempArray), "%I64d", value);
#else
  snprintf(tempArray, sizeof(tempArray), "%lli", value);
#endif

  return tempArray;
}


// ************************************************************************** //
// UTOS                                                                       //
// ************************************************************************** //
string util::utos(const uint32_t value) {
  char tempArray[12];
  snprintf(tempArray, sizeof(tempArray), "%u", value);

  return tempArray;
}


// ************************************************************************** //
// DTOS                                                                       //
// ************************************************************************** //
string util::dtos(const double & value, const int zeros) {
  string result;
  char tempArray[20];
  snprintf(tempArray, sizeof(tempArray), "%lf", value);
  result = tempArray;

  if (0 >= zeros) {
    const size_t decimalPos = result.find('.');
    if (!zeros || result.substr(decimalPos + 1) == "000000") {
      result = result.substr(0, decimalPos);
    }
  } else if (6 > zeros) {
    result = result.substr(0, result.length() - (6 - zeros));
  }

  return result;
}


// ************************************************************************** //
// OCTAL                                                                      //
// ************************************************************************** //
string util::octal(const uint32_t value) {
  char tempArray[12];

  // With values more than zero, prints a 0 before value.                     //
  if (0 == value) {
    snprintf(tempArray, sizeof(tempArray), "0");
  } else {
    snprintf(tempArray, sizeof(tempArray), "0%o", value);
  }

  return tempArray;
}
