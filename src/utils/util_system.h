/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_UTILS_UTIL_SYSTEM_H_
#define SRC_UTILS_UTIL_SYSTEM_H_

#include <stdint.h>
#include <string>

using std::string;

//!
//! @brief   Namespace with a set of jafstutils functions.
//! @author  Jose Antonio Fuentes Santiago
//! @version 1.0
//!
namespace util {
  //!
  //! @brief   Current thread sleeps for specified number of miliseconds.
  //! @details Causes the currently executing thread to sleep for the specified
  //!          number of milliseconds, subject to the precision and accuracy of
  //!          system timers and schedulers. Avoid problems with usleep
  //!          function under Windows.
  //! @param   time  The length of time to sleep in milliseconds.
  //!
  void ssleep(const uint32_t time);

  //!
  //! @brief  Saves the current proccess PID into a file with received filename.
  //! @param  filename  String with full path of filename where stores the PID.
  //!
  void savePid(const string & filename);

  //!
  //! @brief  Executes the received command in system, and returns the output.
  //! @param  command  String with command to execute.
  //! @return String with command execution result. By default the return is
  //!         the content of standard out.
  //!
  string executeCommand(const string & command);
}  // namespace util

#endif  // SRC_UTILS_UTIL_SYSTEM_H_
