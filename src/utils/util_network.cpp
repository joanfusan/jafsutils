/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./util_network.h"

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#ifdef JAFS_LINUX
#include <net/if.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#else
#include <winsock.h>
#include <winsock2.h>
#include <iphlpapi.h>
#endif
#include <string>
#include <map>

#include "./util_number.h"
#include "./util_string.h"

using std::string;
using std::map;


#ifndef JAFS_LINUX
int inet_pton4(const char *src, char *dst);
#endif

namespace util {
  //!
  //! Load IP address of gateways for received interfaces.
  //! @param  interfaces  List that contains the interfaces.
  //! @todo   Only works in Linux, future versions with Windows support.
  //!
  void loadGateways(linterfaces & interfaces) {
    if (!interfaces.empty()) {
      FILE * pipe = popen("route -n", "r");

      if (pipe) {
        string result;
        char data[256];

        // Reads the execution result.                                        //
        while (!feof(pipe)) {
          if (fgets(data, sizeof(data), pipe)) {
            result += data;
          }
        }

        lstring lines;
        mstring gateways;
        split(result, '\n', lines);

        // Iterates over the command lines to extract gateways data.          //
        for (lstring::iterator it = lines.begin(); it != lines.end(); ++it) {
          string current = *it;
          if (isdigit(current[0])) {    // Route line when starts with digit. //
            lstring columns;
            split(current, ' ', columns);
            if (columns.size() == 8) {
              string gateway = *(++columns.begin());

              if (gateway != "0.0.0.0") {
                gateways[*columns.rbegin()] = gateway;
              }
            }
          }
        }

        // Assign the gateways to the interfaces.                             //
        for (util::linterfaces::iterator it = interfaces.begin();
             it != interfaces.end(); ++it) {
          it->gateway = gateways[it->name];
        }

        pclose(pipe);
      }
    }
  }
}  // namespace util


// ************************************************************************** //
// ARRAY_TO_HEX                                                               //
// ************************************************************************** //
string util::arrayToHex(const char * data, const size_t start,
                        const size_t end) {
  string result;

  // Checks the array and values.                                             //
  if (data && end > start) {
    char temp[4];

    for (size_t i = start; i < end; ++i) {
      memset(temp, 0, sizeof(temp));
      snprintf(temp, sizeof(temp), "%02X ", (u_char) data[i]);
      result += temp;
    }

    if (!result.empty()) {
      result.erase(result.size() - 1);
    }
  }

  return result;
}


// ************************************************************************** //
// GET_IP                                                                     //
// ************************************************************************** //
uint32_t util::getIp() {
  uint32_t address = 0;
#ifdef JAFS_LINUX
  struct ifaddrs * interfaces = NULL;
  struct ifaddrs * current = NULL;


  // Gets the interfaces list.                                                //
  getifaddrs(&interfaces);

  if (interfaces) {
    for (current = interfaces; current; current = current->ifa_next) {
      // Checks if current interface has and IPv4 IP.                         //
      if (current->ifa_addr->sa_family == AF_INET) {
        // If IP address is valid, gets the value.                            //
        if ((((struct sockaddr_in *) current->ifa_addr)->sin_addr.s_addr)) {
          address =
                   ((struct sockaddr_in *) current->ifa_addr)->sin_addr.s_addr;
          break;
        }
      }
    }

    freeifaddrs(interfaces);
  }
#endif

  return address;
}


// ************************************************************************** //
// IS_VALID_IP                                                                //
// ************************************************************************** //
bool util::isValidIp(const string & ip) {
  struct sockaddr_in address;
  return inet_pton4(ip.c_str(), (char *) &(address.sin_addr));
}


#ifdef JAFS_LINUX
// ************************************************************************** //
// GET_INTERFACES                                                             //
// ************************************************************************** //
void util::getInterfaces(linterfaces & interfaces) {
  struct ifaddrs * pInterfaces = NULL;
  map<int32_t, net_iface> interfacesMap;

  if (!interfaces.empty()) {
    interfaces.clear();
  }

  // Gets the interaces list.                                                 //
  if (0 == getifaddrs(&pInterfaces) && pInterfaces) {
    struct ifaddrs * interface = NULL;
    const int connection = socket(AF_INET, SOCK_DGRAM, 0);

    // Iterates over network interfaces list.                                 //
    for (interface = pInterfaces; interface; interface = interface->ifa_next) {
      if (interface->ifa_addr) {
        struct ifreq ifaceData;
        net_iface current;

        current.name = interface->ifa_name;
        current.index = if_nametoindex(interface->ifa_name);

        // Gets the IPv4 of the interface.                                    //
        ifaceData.ifr_addr.sa_family = AF_INET;
        strncpy(ifaceData.ifr_name, interface->ifa_name, IFNAMSIZ - 1);
        ioctl(connection, SIOCGIFADDR, &ifaceData);
        current.ip = inet_ntoa(((struct sockaddr_in *)
                                 &ifaceData.ifr_addr)->sin_addr);

        // Gets the MAC address.                                              //
        ioctl(connection, SIOCGIFHWADDR, &ifaceData);
        current.mac = getMac(ifaceData.ifr_hwaddr.sa_data,
                              sizeof(ifaceData.ifr_hwaddr.sa_data));

        // Gets the network mask.                                             //
        ioctl(connection, SIOCGIFNETMASK, &ifaceData);
        current.netmask = inet_ntoa(((struct sockaddr_in *)
                                       &ifaceData.ifr_addr)->sin_addr);

        // Overwrites network interfaces.                                     //
        interfacesMap[current.index] = current;
      }
    }

    // Clean used data.                                                       //
    freeifaddrs(pInterfaces);
    shutdown(connection, SHUT_RDWR);
    close(connection);

    // Dump the interfaces map to the list.                                   //
    for (map<int32_t, net_iface>::iterator it = interfacesMap.begin();
         it != interfacesMap.end(); ++it) {
      interfaces.push_back(it->second);
    }
    loadGateways(interfaces);
  }
}
#endif


// ************************************************************************** //
// GET_MAC                                                                    //
// ************************************************************************** //
string util::getMac(const char * data, const size_t size) {
  string mac;

  if (data && 6 <= size) {
    for (int i = 0; i < 6; ++i) {
      char hexa[4];
      snprintf(hexa, sizeof(hexa), "%02X:", (u_char) data[i]);
      mac += hexa;
    }

    mac.erase(mac.size() - 1);
  }

  return mac;
}


#ifndef JAFS_LINUX

#define NS_INADDRSZ  4
#define NS_IN6ADDRSZ 16
#define NS_INT16SZ   2

int inet_pton4(const char *src, char *dst)
{
    uint8_t tmp[NS_INADDRSZ], *tp;

    int saw_digit = 0;
    int octets = 0;
    *(tp = tmp) = 0;

    int ch;
    while ((ch = *src++) != '\0')
    {
        if (ch >= '0' && ch <= '9')
        {
            uint32_t n = *tp * 10 + (ch - '0');

            if (saw_digit && *tp == 0)
                return 0;

            if (n > 255)
                return 0;

            *tp = n;
            if (!saw_digit)
            {
                if (++octets > 4)
                    return 0;
                saw_digit = 1;
            }
        }
        else if (ch == '.' && saw_digit)
        {
            if (octets == 4)
                return 0;
            *++tp = 0;
            saw_digit = 0;
        }
        else
            return 0;
    }
    if (octets < 4)
        return 0;

    memcpy(dst, tmp, NS_INADDRSZ);

    return 1;
}
#endif
