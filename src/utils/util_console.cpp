/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./util_console.h"

#ifdef JAFS_LINUX

#include <stdio.h>
#include <string.h>
#include <string>

using std::string;


//! Character scape for consola formats.                                      //
const char ESC_CHAR = 27;
//! Default format string size.                                              //
const size_t FORMAT_SIZE = 10;

// ************************************************************************** //
// STR_FORMAT                                                                 //
// ************************************************************************** //
string util::strFormat(const console_formats format) {
  char result[FORMAT_SIZE];

  memset(result, 0, sizeof(result));
  snprintf(result, sizeof(result), "%c[%im", ESC_CHAR, format);

  return result;
}

#endif
