/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_UTILS_UTIL_CONSOLE_H_
#define SRC_UTILS_UTIL_CONSOLE_H_

#ifdef JAFS_LINUX

#include <string>

using std::string;


//!
//! @brief   Namespace with a set of jafstutils functions.
//! @author  Jose Antonio Fuentes Santiago
//! @version 1.0
//!
namespace util {
  //!
  //! @brief  Enumeration with text formats
  //!
  typedef enum {
    //! Text with normal style.
    FRM_NORMAL = 0,
    //! Text in bold style.
    FRM_BOLD,
    //! Dark text.
    FRM_DARK,
    //! Underline text.
    FRM_UNDERLINE = 4,
    //! Mark text as selected.
    FRM_REVERSE = 7,
    //! Strike text.
    FRM_STRIKED = 9,
    //! Black text colour.
    FRM_BLACK = 30,
    //! Red text colour.
    FRM_RED,
    //! Green text colour.
    FRM_GREEN,
    //! Yellow text colour.
    FRM_YELLOW,
    //! Blue text colour.
    FRM_BLUE,
    //! Pink text colour.
    FRM_MAGENTA,
    //! Cyan text colour.
    FRM_CYAN,
    //! White text colour.
    FRM_WHITE,
    //! Background red colour.
    FRM_BACK_RED = 41,
    //! Background green colour.
    FRM_BACK_GREEN,
    //! Background yellow colour.
    FRM_BACK_YELLOW,
    //! Background blue colour.
    FRM_BACK_BLUE,
    //! Background pnk colour.
    FRM_BACK_MAGENTA,
    //! Background cyan colour.
    FRM_BACK_CYAN,
    //! Background grey colour.
    FRM_BACK_GREY
  } console_formats;

  //!
  //! @brief  Gets a string with Sets the text format with received value.
  //! @param  format  Text format to appy.
  //! @return A string with the format.
  //!
  string strFormat(const console_formats format);
}  // namespace util
#endif

#endif  // SRC_UTILS_UTIL_CONSOLE_H_
