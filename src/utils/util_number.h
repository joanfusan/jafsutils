/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_UTILS_UTIL_NUMBER_H_
#define SRC_UTILS_UTIL_NUMBER_H_

#include <stddef.h>
#include <stdint.h>
#include <sys/types.h>
#include <typeinfo>
#include <string>

using std::string;


//!
//! @brief   Namespace with a set of jafstutils functions.
//! @author  Jose Antonio Fuentes Santiago
//! @version 1.0
//!
namespace util {
  //!
  //! Returns a boolean value that indicates if reveived data has integer type.
  //! @param  data  Value to analyze.
  //! @return Boolean value that indicates if reveived data has integer type.
  //!
  template<class T>
  bool isInteger(T & data) {
    return typeid(T) == typeid(int) ||
#ifdef JAFS_LINUX
           typeid(T) == typeid(uint) ||
#endif
           typeid(T) == typeid(int32_t) ||
           typeid(T) == typeid(uint32_t) ||
           typeid(T) == typeid(short) ||
#ifdef JAFS_LINUX
           typeid(T) == typeid(ushort) ||
#endif
           typeid(T) == typeid(int16_t) ||
           typeid(T) == typeid(uint16_t) ||
           typeid(T) == typeid(long) ||
#ifdef JAFS_LINUX
           typeid(T) == typeid(ulong) ||
#endif
           typeid(T) == typeid(int64_t) ||
           typeid(T) == typeid(uint64_t);
  }


  //!
  //! Get an integer value from a char array.
  //! @param  data     Char array from extract the value.
  //! @param  size     Size of array.
  //! @param  value    Variable that will stores the value.
  //! @param  reverse  Boolean value that indicates if we need to revert the
  //!                  array data.
  //! @return Boolean value that indicates if all is ok.
  //!
  template<class T>
  bool arrayToInteger(char * data, size_t size, T & value,
                      bool reverse = false) {
    bool ok = false;

    // Checks if array is not null, size of value is valid and received       //
    // value variable has integer type.                                       //
    if (data && sizeof(T) <= size && isInteger(value)) {
      // In reverse mode, an array with reverse value is created.             //
      if (reverse) {
        char * second = new char[size];
        for (size_t i = 0; i < size; ++i) {
          second[size - i - 1] = data[i];
        }
        value = *(reinterpret_cast<T *> (second));
        delete(second);
      } else {
        value = *(reinterpret_cast<T *> (data));
      }

      ok = true;
    }

    return ok;
  }


  //!
  //! Returns a string with received boolean value.
  //! @param  value  Boolean value to convert into string.
  //! @return String with boolean value.
  //!
  string btos(const bool value);

  //!
  //! Returns a string with received integer value.
  //! @param  value  Integer value to convert into string.
  //! @return String with integer value.
  //!
  string itos(const int32_t value);

  //!
  //! Returns a string with received long value.
  //! @param  value  Long value to convert into string.
  //! @return String with long value.
  //!
  string ltos(const int64_t & value);

  //!
  //! Returns a string with received unsigned integer value.
  //! @param  value  Unsigned integer value to convert into string.
  //! @return String with unsigned integer value.
  //!
  string utos(const uint32_t value);

  //!
  //! Returns a string with received double value.
  //! @param  value  Double value to convert into string.
  //! @param  zeros  Number of zeros after decimal point. A negative value show
  //!                only decimal value when is not 0. A value between 0 and 6
  //!                show this number of zeros.
  //! @return String with double value.
  //!
  string dtos(const double & value, const int zeros = -1);

  //!
  //! Returns a string with representation in octal of received value.
  //! @param  value  Unsigned integer value to convert.
  //! @return String with octal value representation.
  //!
  string octal(const uint32_t value);
}  // namespace util

#endif  // SRC_UTILS_UTIL_NUMBER_H_
