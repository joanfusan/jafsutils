/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#include "./util_file.h"
#include "../jafsutils.h"

#ifdef JAFS_WIN
#include <io.h>
#endif

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <string>

#include "util_number.h"

using std::string;

//! Maximun value for mode.                                                   //
const uint32_t MAX_MODE = 0777;


// ************************************************************************** //
// FEXISTS                                                                    //
// ************************************************************************** //
bool util::fexists(const string & filename) {
  // Si el resultado es -1 no existe el fichero.                              //
  struct stat fileInfo;
  return (stat(filename.c_str(), &fileInfo) != -1);
}


// ************************************************************************** //
// FSIZE                                                                      //
// ************************************************************************** //
bool util::fsize(const string & filename, size_t & size) {
  bool ok = false;
  size = 0;

  /* Si se pudo obtener dicha información, se extrae el tamaño.               */
  struct stat fileInfo;
  if (-1 != stat(filename.c_str(), &fileInfo)) {
    size = fileInfo.st_size;
    ok = true;
  }

  return ok;
}


// ************************************************************************** //
// MAKE_DIR                                                                   //
// ************************************************************************** //
bool util::makedir(const string & path, const uint32_t mode) {
  bool result = false;

  // When directory exists, if a mode is received, only change the mode.      //
  if (fexists(path)) {
    result = true;
    if (0777 >= mode) {
      result = !chmod(path.c_str(), mode);
    }
  } else {
#ifdef JAFS_LINUX
    // Makes the command in linux format.                                     //
    string command = "mkdir -p ";

    if (MAX_MODE >= mode) {
      command += "-m " + octal(mode) + " ";
    }

    command += path + " 2>&1 >/dev/null";

    FILE * pipe = popen(command.c_str(), "r");
    if (pipe) {
      string output;

      while (!feof(pipe)) {
        char aux[256];
        memset(aux, 0, sizeof(aux));
        if (fgets(aux, sizeof(aux), pipe)) {
          output += string(aux);
        }
      }

      pclose(pipe);

      // If result is empty, there is no error.                                 //
      result = output.empty();
    }
#else
  // TODO(jafs) not yet implemented.
#endif
  }

  return result;
}
