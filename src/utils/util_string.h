/*
 * Project: jafsutils
 * Copyright 2013 JAFS.es
 */
#ifndef SRC_UTILS_UTIL_STRING_H_
#define SRC_UTILS_UTIL_STRING_H_

#include <string>
#include "../objects/structures.h"

using std::string;


//!
//! @brief   Namespace with a set of jafstutils functions.
//! @author  Jose Antonio Fuentes Santiago
//! @version 1.0
//!
namespace util {
  //! Constant that defines parameters separator in strings.
  const char PARAMS_SEPARATOR = '\n';
  //! Constant that defines values separator in strings.
  const char VALUES_SEPARATOR = '=';

  //!
  //! @brief  Obtains a string with the string pair values received.
  //! @param  data  Map of strings to convert.
  //! @return String with extracted values.
  //!
  string mtos(mstring & data);

  //!
  //! @brief  Gets a map of string from a formated string.
  //! @param  data    Strings to extract.
  //! @param  result  Map with results of conversion.
  //!
  void stom(const string & data, mstring & result);

  //!
  //! @brief  Removes the black spaces in the received string and returns the
  //!         result in a new string.
  //! @param  data  String to clean.
  //! @return String with the result.
  //!
  string trim(const string & data);

  //!
  //! @brief  Splits the received string around the indicated character.
  //! @param  data       String to split.
  //! @param  separator  Character that indicates separation in string.
  //! @param  result     List of string generated.
  //!
  void split(const string & data, const char separator, lstring & result);

  //!
  //! @brief  Replaces all occurrences of oldChar in received string with
  //!         newChar. The received string will be overwrited.
  //! @param  oldChar  The old character.
  //! @param  newChar  The new character.
  //! @param  data     The string in which replace the characters.
  //!
  void replace(string & data, const char oldChar, const char newChar);

  //!
  //! @brief  Returns the index within this string of the first occurrence of
  //!         the specified substring.
  //! @param  data       String into search.
  //! @param  substring  The substring to search for.
  //! @return The index within this string of the first occurrence of the
  //!         specified substring. If no such value of the subtring exists,
  //!         then -1 is returned.
  //!
  int indexOf(const string & data, const string & substring);

  //!
  //! Receives a string and returns the string in lowercases.
  //! @param  data  String to convert.
  //! @return String in lowercase.
  //!
  string lowercase(const string & data);

  //!
  //! Receives a string and returns the string in uppercase.
  //! @param  data  String to convert.
  //! @return String in uppercase.
  //!
  string uppercase(const string & data);

  //!
  //! Tests if this string starts with the specified prefix.
  //! @param  data    String in which search the prefix.
  //! @param  prefix  The prefix to search
  //! @return <code>true</code> if the second string is a prefix of the firts
  //!         string; <code>false</code> otherwise. Note also that true will be
   //!        returned if the argument is an empty string or is equal to the
   //!        constent string.
  bool startsWith(const string & data, const string & prefix);

  //!
  //! Tests if this string starts with the specified prefix ignoring case.
  //! @param  data    String in which search the prefix.
  //! @param  prefix  The prefix to search
  //! @return <code>true</code> if the second string is a prefix of the firts
  //!         string; <code>false</code> otherwise. Note also that true will be
   //!        returned if the argument is an empty string or is equal to the
   //!        constent string.
  bool startsWithCase(const string & data, const string & prefix);
}  // namespace util

#endif  // SRC_UTILS_UTIL_STRING_H_
