# JafsUtils library

JafsUtils is a library developed in C + +, which contains a set of utilities to make life easier for developers.

On the one hand, it has functions grouped by category: **console**, **file**, **network**, **number**, **string** and **system**. On the other, has some classes for working with **dates**, have a system **logger**, or read **configuration files**.

## Changelog

### 1.0 Version - dec 15 2013

* First version.
